﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace QBO_to_Exigo_XML
{
    class Customer
    {
        private string _customerid;
        private string _customeremail;
        private string _coachid;
        private string _firstname;
        private string _lastname;
        private string _registration;
        private string _type;
        private string _workshop;
        int _rank;

        public string CustomerId
        {
            get { return _customerid; }
            set { _customerid = value; }
        }

        public string CustomerEmail
        {
            get { return _customeremail; }
        }

        public string CoachId
        {
            get { return _coachid; }
            set { _coachid = value; }
        }

        public string FirstName
        {
            get { return _firstname; }
        }

        public string LastName
        {
            get { return _lastname; }
        }

        public string RegistrationNumber
        {
            get { return _registration; }
        }

        public string RegistrationType
        {
            get { return _type; }
        }

        public string WorkShop
        {
            get { return _workshop; }
        }

        public void Load(DataRow row)
        {
            
            
            _registration = row["Confirmation Number"].ToString();
            _type = row["Registration Type"].ToString();
            _customerid = row["Health Coach ID"].ToString();

            if (_customerid != null)
            {

                SqlConnection conn = new SqlConnection();
                SqlCommand command = new SqlCommand();
                SqlDataReader sqlreader;
                string connstring = "Data Source=EXIGOPROD2\\EXPROD,7095;Initial Catalog=ExigoSyncSQL_PRD;Integrated Security=True";
                conn.ConnectionString = connstring;
                command = new SqlCommand("SELECT Field2 FROM Customers WHERE Field1 = @field1", conn);
                command.Parameters.Add("@field1", SqlDbType.NVarChar);
                command.CommandTimeout = 300;

                conn.Open();

                command.Parameters["@field1"].Value = _customerid;
                sqlreader = command.ExecuteReader();

                while (sqlreader.Read())
                {
                    _coachid = sqlreader.GetValue(0).ToString();
                }

                conn.Close();

                _firstname = row["First Name"].ToString();
                _lastname = row["Last Name"].ToString();
                //_customerid = row["Health Coach ID"].ToString();
                _customeremail = row["Email Address"].ToString();
            }

            else
            {
                if (_firstname.Equals(null))
                {
                    _firstname = "Company";
                    _lastname = "Account";
                    _customeremail = "";
                }
                else
                {
                    _firstname = row["First Name"].ToString();
                    _lastname = row["Last Name"].ToString();
                    _customeremail = row["Email Address"].ToString();
                }               
                
                _customerid = "5";                
                _coachid = "5";
            }
        }

        public void LookupByEmail(DataRow row)
        {
            _customeremail = row["Email Address"].ToString();

            SqlConnection conn = new SqlConnection();
            SqlCommand command = new SqlCommand();
            SqlDataReader sqlreader;
            string connstring = "Data Source=EXIGOPROD2\\EXPROD,7095;Initial Catalog=ExigoSyncSQL_PRD;Integrated Security=True";
            conn.ConnectionString = connstring;
            
            command = new SqlCommand("SELECT Field1, FirstName, LastName, Email FROM Customers WHERE Email = @email", conn);
            command.Parameters.Add("@email", SqlDbType.NVarChar);
            command.CommandTimeout = 300;

            if (conn.State == ConnectionState.Closed) { conn.Close(); }
            conn.Open();

            command.Parameters["@email"].Value = _customeremail;
            sqlreader = command.ExecuteReader();

            while (sqlreader.Read())
            {
                _customerid = sqlreader.GetValue(0).ToString();
                _firstname = sqlreader.GetValue(1).ToString();
                _lastname = sqlreader.GetValue(2).ToString();                
            }
            conn.Close();
        }

        public void LookupByName(DataRow row)
        {
            _firstname = row["First Name"].ToString();
            _lastname = row["Last Name"].ToString();

            SqlConnection conn = new SqlConnection();
            SqlCommand command = new SqlCommand();
            SqlDataReader sqlreader;
            string connstring = "Data Source=EXIGOPROD2\\EXPROD,7095;Initial Catalog=ExigoSyncSQL_PRD;Integrated Security=True";
            conn.ConnectionString = connstring;
            command = new SqlCommand("SELECT Field1, FirstName, LastName, Email FROM Customers WHERE FirstName = @firstname AND LastName = @lastname", conn);
            command.Parameters.Add("@firstname", SqlDbType.NVarChar);
            command.Parameters.Add("@lastname", SqlDbType.NVarChar);
            command.CommandTimeout = 300;

            conn.Open();

            command.Parameters["@firstname"].Value = _firstname;
            command.Parameters["@lastname"].Value = _lastname;
            sqlreader = command.ExecuteReader();

            while (sqlreader.Read())
            {
                _customerid = sqlreader.GetValue(0).ToString();                
                _customeremail = sqlreader.GetValue(3).ToString();
            }
            conn.Close();
        }

        public int GetWorkshop(string customerid)
        {
            if (customerid == "5" || customerid == "")
            {
                _rank = 0;
            }
            else
            {


                _rank = 0;
                SqlConnection conn = new SqlConnection();
                SqlCommand command = new SqlCommand();
                SqlDataReader sqlreader;
                string connstring = "Data Source=EXIGOPROD2\\EXPROD,7095;Initial Catalog=ExigoSyncSQL_PRD;Integrated Security=True";
                conn.ConnectionString = connstring;
                command = new SqlCommand("SELECT Current_Rank FROM V_Tree_View tv JOIN Customers cus ON cus.CustomerID = tv.Exigo_CustomerID WHERE cus.Field1 = @field1 AND tv.PeriodID = 13", conn);
                command.Parameters.Add("@field1", SqlDbType.NVarChar);
                command.CommandTimeout = 300;

                conn.Open();

                command.Parameters["@field1"].Value = _customerid;
                sqlreader = command.ExecuteReader();

                while (sqlreader.Read())
                {
                    //_workshop = sqlreader.GetValue(0).ToString();
                    _rank = Convert.ToInt32(sqlreader.GetValue(0));
                }

                conn.Close();
            }

            return _rank;
        }
    }
}

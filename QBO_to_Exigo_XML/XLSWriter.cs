﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
using System.Data;
using System.Windows.Forms;


namespace QBO_to_Exigo_XML
{
    class XLSWriter
    {
        
        private Workbook _book;
        private Worksheet _sheet;
        private Microsoft.Office.Interop.Excel.Application _excel;  
              
        public void WriteXlsx(System.Data.DataTable table, string filename, string sheetname)
        {
            _excel = new Microsoft.Office.Interop.Excel.Application();
            _book = _excel.Workbooks.Add();
            _sheet = _book.ActiveSheet;
            _sheet.Name = sheetname;

            int iColumn = 0;

            foreach (DataColumn c in table.Columns)
            {
                iColumn++;
                _excel.Cells[1, iColumn] = c.ColumnName;
            }

            int iRow = _sheet.UsedRange.Rows.Count - 1;

            foreach (DataRow row in table.Rows)
            {
                iRow++;

                iColumn = 0;

                foreach (DataColumn c in table.Columns)
                {
                    iColumn++;
                    _excel.Cells[iRow + 1, iColumn] = row[c.ColumnName];
                }
            }

            _sheet.Activate();

            _book.SaveAs(Filename: filename);

            if (_book.Saved == true)
            {
                MessageBox.Show(filename + " has been saved.", "File saved", MessageBoxButtons.OK);
            }
            
            _book.Close();            

            _excel.Quit();            

        }
    }
}

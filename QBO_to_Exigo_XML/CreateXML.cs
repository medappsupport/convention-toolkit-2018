﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using System.Globalization;

namespace QBO_to_Exigo_XML
{
    class CreateXML
    {
        private string path = "C:\\POS\\XML\\";
        private string filename;
        public void ExigoXML(Order order, Customer order_customer, List<OrderLine> order_lines)
        {
            filename = order.OrderId + ".xml";
            
            /*Customer order_customer = new Customer();
            order_customer = customers.Find(x => x.CustomerId == order.CustomerId);                        

            List<OrderLine> order_lines = new List<OrderLine>();
            order_lines = orderlines.FindAll(x => x.OrderID == order.OrderId);*/
            Decimal[] linevolume = new Decimal[order_lines.Count];
            linevolume = order_lines.Select(x => x.Volume).ToArray();
            CultureInfo culture = new CultureInfo("ja-JP");
            //DateTime timestamp = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified);
            DateTime timestamp = DateTime.Now;
            
            XmlDocument xmldoc = new XmlDocument();

            XmlDeclaration xmldeclaration = xmldoc.CreateXmlDeclaration("1.0", "utf-8", null);

            XmlElement envelope = xmldoc.CreateElement("Envelope", "http://schemas.xmlsoap.org/soap/envelope/");
            envelope.SetAttribute("xmlns:soapenv", "http://schemas.xmlsoap.org/soap/envelope/");
            envelope.SetAttribute("xmlns:ord", "http://www.medifast.com/common/order");
            envelope.Prefix = "soapenv";
            xmldoc.AppendChild(envelope);



            XmlElement header = xmldoc.CreateElement("Header", "http://schemas.xmlsoap.org/soap/envelope/");
            header.Prefix = "soapenv";
            header.IsEmpty = true;
            envelope.AppendChild(header);

            XmlElement body = xmldoc.CreateElement("Body", "http://schemas.xmlsoap.org/soap/envelope/");
            body.Prefix = "soapenv";

            XmlElement xorder = xmldoc.CreateElement("order", "http://www.medifast.com/common/order");
            xorder.Prefix = "ord";
            body.AppendChild(xorder);

            XmlElement orderid = xmldoc.CreateElement("order-id");
            orderid.AppendChild(xmldoc.CreateTextNode(order.OrderId));
            xorder.AppendChild(orderid);

            XmlElement customerid = xmldoc.CreateElement("customer-id");
            customerid.AppendChild(xmldoc.CreateTextNode(order.CustomerId));
            xorder.AppendChild(customerid);

            XmlElement coachid = xmldoc.CreateElement("override-coach-id");
            coachid.AppendChild(xmldoc.CreateTextNode(order_customer.CoachId));
            xorder.AppendChild(coachid);

            XmlElement source = xmldoc.CreateElement("source");
            source.AppendChild(xmldoc.CreateTextNode("WEB"));
            xorder.AppendChild(source);

            XmlElement ordertype = xmldoc.CreateElement("order-type");
            ordertype.AppendChild(xmldoc.CreateTextNode("Order"));
            xorder.AppendChild(ordertype);

            XmlElement orderdate = xmldoc.CreateElement("order-date");
            orderdate.AppendChild(xmldoc.CreateTextNode(order.OrderDate.ToString("yyyy-MM-dd")));
            xorder.AppendChild(orderdate);

            XmlElement channel = xmldoc.CreateElement("channel");
            channel.AppendChild(xmldoc.CreateTextNode("OPTAVIA"));
            xorder.AppendChild(channel);

            XmlElement shipto = xmldoc.CreateElement("ship-to");

            XmlElement name = xmldoc.CreateElement("name");
            name.AppendChild(xmldoc.CreateTextNode("DoNotShip"));
            shipto.AppendChild(name);

            XmlElement address1 = xmldoc.CreateElement("address-line-1");
            address1.AppendChild(xmldoc.CreateTextNode("100 International Drive"));
            shipto.AppendChild(address1);

            XmlElement address2 = xmldoc.CreateElement("address-line-2");
            address2.AppendChild(xmldoc.CreateTextNode("18th Floor"));
            shipto.AppendChild(address2);

            XmlElement city = xmldoc.CreateElement("city");
            city.AppendChild(xmldoc.CreateTextNode("Baltimore"));
            shipto.AppendChild(city);

            XmlElement state = xmldoc.CreateElement("state");
            state.AppendChild(xmldoc.CreateTextNode("MD"));
            shipto.AppendChild(state);

            XmlElement postal_code = xmldoc.CreateElement("postal-code");
            postal_code.AppendChild(xmldoc.CreateTextNode("21201"));
            shipto.AppendChild(postal_code);

            XmlElement country = xmldoc.CreateElement("country");
            country.AppendChild(xmldoc.CreateTextNode("US"));
            shipto.AppendChild(country);

            xorder.AppendChild(shipto);

            XmlElement currency = xmldoc.CreateElement("currency");
            currency.AppendChild(xmldoc.CreateTextNode("USD"));
            xorder.AppendChild(currency);

            XmlElement subtotal = xmldoc.CreateElement("subtotal");
            subtotal.AppendChild(xmldoc.CreateTextNode(order.SubTotal.ToString("F2")));
            xorder.AppendChild(subtotal);

            XmlElement shipping_charges = xmldoc.CreateElement("shipping-charges");
            shipping_charges.AppendChild(xmldoc.CreateTextNode("0"));
            xorder.AppendChild(shipping_charges);

            XmlElement shipping_surcharges = xmldoc.CreateElement("shipping-surcharges");
            shipping_surcharges.AppendChild(xmldoc.CreateTextNode("0"));
            xorder.AppendChild(shipping_surcharges);

            XmlElement shipping_discount = xmldoc.CreateElement("shipping-discount");
            shipping_discount.AppendChild(xmldoc.CreateTextNode("0"));
            xorder.AppendChild(shipping_discount);

            XmlElement tax = xmldoc.CreateElement("tax");
            tax.AppendChild(xmldoc.CreateTextNode(order.Tax.ToString("F2")));
            xorder.AppendChild(tax);

            XmlElement total = xmldoc.CreateElement("total");
            total.AppendChild(xmldoc.CreateTextNode(order.Total.ToString("F2")));
            xorder.AppendChild(total);

            XmlElement cvolume = xmldoc.CreateElement("commissionable-volume");
            cvolume.AppendChild(xmldoc.CreateTextNode(linevolume.Sum().ToString()));
            xorder.AppendChild(cvolume);

            XmlElement leads = xmldoc.CreateElement("leads-pool-order");
            leads.AppendChild(xmldoc.CreateTextNode("false"));
            xorder.AppendChild(leads);

            XmlElement created_timestamp = xmldoc.CreateElement("created-timestamp");
            created_timestamp.AppendChild(xmldoc.CreateTextNode(timestamp.ToUniversalTime()
                .ToString("yyyy-MM-ddThh:mm:ss.fffZ")));
            xorder.AppendChild(created_timestamp);

            XmlElement paymentmethods = xmldoc.CreateElement("payment-methods");

            XmlElement paymentmethod = xmldoc.CreateElement("ord:payment-method");

            XmlElement type = xmldoc.CreateElement("type");
            type.AppendChild(xmldoc.CreateTextNode("CRED"));
            paymentmethod.AppendChild(type);

            XmlElement provider = xmldoc.CreateElement("provider");
            provider.AppendChild(xmldoc.CreateTextNode("UNKNOWN"));
            paymentmethod.AppendChild(provider);

            XmlElement amount = xmldoc.CreateElement("amount");
            amount.AppendChild(xmldoc.CreateTextNode(order.Total.ToString("F2")));
            paymentmethod.AppendChild(amount);

            paymentmethods.AppendChild(paymentmethod);

            xorder.AppendChild(paymentmethods);

            XmlElement xorderlines = xmldoc.CreateElement("order-lines");
            xmldoc.CreateComment("One or more repetitions of the <order-line>");

            int seq = 0;

            foreach (OrderLine order_line in order_lines)
            {
                XmlElement xorderline = xmldoc.CreateElement("order-line");
                seq = seq + 1;

                XmlElement seq_number = xmldoc.CreateElement("seq-number");
                seq_number.AppendChild(xmldoc.CreateTextNode(seq.ToString()));
                xorderline.AppendChild(seq_number);

                XmlElement sku_id = xmldoc.CreateElement("sku-id");
                sku_id.AppendChild(xmldoc.CreateTextNode(order_line.SKU));
                xorderline.AppendChild(sku_id);

                XmlElement uom = xmldoc.CreateElement("uom");
                uom.AppendChild(xmldoc.CreateTextNode(order_line.UOM));
                xorderline.AppendChild(uom);

                XmlElement description = xmldoc.CreateElement("Description");                
                description.AppendChild(xmldoc.CreateTextNode(Regex.Replace(order_line.Description, "&","and")));
                xorderline.AppendChild(description);

                XmlElement list_price = xmldoc.CreateElement("list-price");
                list_price.AppendChild(xmldoc.CreateTextNode(order_line.ProductPrice.ToString("F2")));
                xorderline.AppendChild(list_price);

                XmlElement sale_price = xmldoc.CreateElement("sale-override-price");
                sale_price.IsEmpty = true;
                xorderline.AppendChild(sale_price);

                XmlElement qty = xmldoc.CreateElement("qty");
                qty.AppendChild(xmldoc.CreateTextNode(order_line.Quantity.ToString()));
                xorderline.AppendChild(qty);

                XmlElement total_discount = xmldoc.CreateElement("total_discount");
                total_discount.IsEmpty = true;
                xorderline.AppendChild(total_discount);

                XmlElement line_tax = xmldoc.CreateElement("tax");
                line_tax.AppendChild(xmldoc.CreateTextNode(order_line.Tax.ToString("F2")));
                xorderline.AppendChild(line_tax);

                XmlElement line_total = xmldoc.CreateElement("total");
                line_total.AppendChild(xmldoc.CreateTextNode(order_line.Total.ToString("F2")));
                xorderline.AppendChild(line_total);

                XmlElement line_volume = xmldoc.CreateElement("commissionable-volume");
                line_volume.AppendChild(xmldoc.CreateTextNode(linevolume[seq - 1].ToString("F2")));
                xorderline.AppendChild(line_volume);

                XmlElement reward = xmldoc.CreateElement("reward-earned");
                reward.AppendChild(xmldoc.CreateTextNode("0"));
                xorderline.AppendChild(reward);

                xorderlines.AppendChild(xorderline);
            }

            xorder.AppendChild(xorderlines);
            body.AppendChild(xorder);
            envelope.AppendChild(body);

            xmldoc.Save(path + filename);
        }

        public void NavXML(Order order, Customer order_customer, List<OrderLine> order_line)
        {
            filename = order.OrderId + "_NAV.xml";
            XmlWriterSettings xmlsettings = new XmlWriterSettings();
            xmlsettings.Indent = true;

            using (XmlWriter xmlwrite = XmlWriter.Create(path + "Nav\\Ind\\" + filename, xmlsettings))
            {
                xmlwrite.WriteStartDocument();
                xmlwrite.WriteStartElement("Envelope", "");
                xmlwrite.WriteStartElement("Body");
                xmlwrite.WriteStartElement("ordersResponse");
                xmlwrite.WriteStartElement("navisionOrders");

                xmlwrite.WriteStartElement("navisionOrder");

                xmlwrite.WriteElementString("orderNo", order.OrderId);
                xmlwrite.WriteElementString("documentType", "Order");
                xmlwrite.WriteElementString("ORHRefID", "");

                xmlwrite.WriteStartElement("autoShip");
                xmlwrite.WriteElementString("shippingCode", "DNS");
                xmlwrite.WriteElementString("shippingCodeFrequencyDays", "");
                xmlwrite.WriteElementString("initialShipDate", "");
                xmlwrite.WriteElementString("lastShipDate", "");
                xmlwrite.WriteElementString("nextShipDate", "");
                xmlwrite.WriteElementString("autoShipEnabled", "false");
                xmlwrite.WriteElementString("autoShipType", "");
                xmlwrite.WriteEndElement(); //End autoShip

                xmlwrite.WriteStartElement("clientInfo");
                xmlwrite.WriteElementString("clientNo", order_customer.CustomerId);
                xmlwrite.WriteElementString("email", order_customer.CustomerEmail);
                xmlwrite.WriteElementString("firstName", order_customer.FirstName);
                xmlwrite.WriteElementString("lastName", order_customer.LastName);
                xmlwrite.WriteElementString("phone", "");
                xmlwrite.WriteElementString("eveningPhone", "");
                xmlwrite.WriteElementString("weightToLose", "");
                xmlwrite.WriteElementString("howDidYouHear", "");
                xmlwrite.WriteElementString("shippingFirstName", order_customer.FirstName);
                xmlwrite.WriteElementString("shippingLastName", order_customer.LastName);
                xmlwrite.WriteElementString("shippingAddr", "701 Convention Plaza");
                xmlwrite.WriteElementString("shippingAddr2", "");
                xmlwrite.WriteElementString("shippingCity", "St. Louis");
                xmlwrite.WriteElementString("shippingState", "MO");
                xmlwrite.WriteElementString("shippingZip", "63101");
                xmlwrite.WriteElementString("shippingCountry", "US");
                xmlwrite.WriteElementString("billingFirstName", order_customer.FirstName);
                xmlwrite.WriteElementString("billingLastName", order_customer.LastName);
                xmlwrite.WriteElementString("billingAddr", "");
                xmlwrite.WriteElementString("billingAddr2", "");
                xmlwrite.WriteElementString("billingCity", "");
                xmlwrite.WriteElementString("billingState", "");
                xmlwrite.WriteElementString("billingZip", "");
                xmlwrite.WriteElementString("billingCountry", "");
                xmlwrite.WriteEndElement(); //End clientInfo

                xmlwrite.WriteElementString("shipmentDate", "");
                xmlwrite.WriteElementString("orderSource", "POS");
                xmlwrite.WriteElementString("leadType", "POS");
                xmlwrite.WriteElementString("custPostGrp", "TSFL");
                xmlwrite.WriteElementString("leadsOrder", "0");
                xmlwrite.WriteElementString("adjustments", "");
                xmlwrite.WriteElementString("orderDateTime", order.OrderDate.ToString());

                xmlwrite.WriteStartElement("shipping");
                xmlwrite.WriteElementString("shipAmount", "");
                xmlwrite.WriteElementString("shipDiscountAmount", "");
                xmlwrite.WriteElementString("shipTax", "");
                xmlwrite.WriteElementString("shipCode", "");
                xmlwrite.WriteElementString("shipProcessCode", "");
                xmlwrite.WriteEndElement(); //End shipping

                xmlwrite.WriteElementString("taxAmount", "");
                xmlwrite.WriteElementString("taxSource", "NO_NEXUS");
                xmlwrite.WriteElementString("orderTotalAmount", order.Total.ToString("F2"));

                xmlwrite.WriteStartElement("sponsorInfo");
                xmlwrite.WriteElementString("sponsorId", "");
                xmlwrite.WriteElementString("sponsorName", "");
                xmlwrite.WriteElementString("sponsorEmail", "");
                xmlwrite.WriteElementString("sponsorWebsite", "");
                xmlwrite.WriteElementString("sponsorPhone", "");
                xmlwrite.WriteEndElement(); //End sponsorInfo

                xmlwrite.WriteElementString("comments", "");
                xmlwrite.WriteElementString("packingSlipComments", "");

                xmlwrite.WriteStartElement("paymentDetails");
                xmlwrite.WriteStartElement("paymentDetail");
                xmlwrite.WriteElementString("authorizationNum", "");
                xmlwrite.WriteElementString("ccNum", "");
                xmlwrite.WriteElementString("cvvNum", "");
                xmlwrite.WriteElementString("paymentTerms", "CASH");
                xmlwrite.WriteElementString("paymentMethod", "");
                xmlwrite.WriteElementString("paymentAmount", "");
                xmlwrite.WriteEndElement(); //End paymentDetail
                xmlwrite.WriteEndElement(); //End paymentDetails

                xmlwrite.WriteStartElement("orderItemLines");

                foreach (OrderLine line in order_line)
                { 

                    xmlwrite.WriteStartElement("orderItem");
                    xmlwrite.WriteElementString("lineItem", "");
                    xmlwrite.WriteElementString("orderItemNo", line.OrderID);
                    xmlwrite.WriteElementString("lineSku", line.SKU.Split('_')[0]);
                    xmlwrite.WriteElementString("lineQuantity", line.Quantity.ToString());
                    xmlwrite.WriteElementString("unitOfMeasure", line.UOM);
                    xmlwrite.WriteElementString("unitPrice", line.ListPrice.ToString("F2"));
                    xmlwrite.WriteElementString("cashOnAccountClientNo", "");
                    xmlwrite.WriteElementString("lineDiscount", "0");
                    xmlwrite.WriteElementString("lineTaxAmount", "0");
                    xmlwrite.WriteEndElement(); //End OrderItem
                }

                xmlwrite.WriteEndElement(); //End OrderItemLines
                xmlwrite.WriteEndElement(); //End navisionOrder

                xmlwrite.WriteEndElement(); //End navisionOrders

                xmlwrite.WriteStartElement("message");
                xmlwrite.WriteElementString("success", "true");
                xmlwrite.WriteElementString("code", "NV3");
                xmlwrite.WriteElementString("description", "MaxOrders exceed orders returned - end of orders");
                xmlwrite.WriteEndElement(); //End message

                xmlwrite.WriteEndElement(); //End ordersResponse
                xmlwrite.WriteEndElement(); //End Body
                xmlwrite.WriteEndDocument();

                //xmlwrite.Finalize;
                xmlwrite.Flush();
                xmlwrite.Close();
            };
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace QBO_to_Exigo_XML
{
    class Order
    {
        private string _orderid;
        private string _customerid;
        private DateTime _orderdate;
        //private DateTime _timestamp;
        private Decimal _subtotal;
        private Decimal _tax;
        private Decimal _total;

        public string OrderId
        {
            get { return _orderid; }
            set { _orderid = value; }
        }

        public string CustomerId
        {
            get { return _customerid; }
            set { _customerid = value; }
        }               

        public DateTime OrderDate
        {
            get { return _orderdate; }
            set { _orderdate = value; }
        }
        
        /*public DateTime TimeStamp
        {
            get { return _timestamp; }
            set { _timestamp = value; }
        }*/      

        public Decimal SubTotal
        {
            get { return _subtotal; }
            set { _subtotal = value; }
        }

        public Decimal Tax
        {
            get { return _tax; }
            set { _tax = value; }
        }

        public Decimal Total
        {
            get { return _total; }
            set { _total = value; }
        }

        public void Load(DataRow row)
        {
            string[] customer = (row["Customer"].ToString()).Split(' ');
            _orderid = "C2018" + row["Num"].ToString();
            _orderdate = DateTime.Parse(row["Date"].ToString());
            //_timestamp = DateTime.Now;
            _customerid = customer[customer.Length - 1];
            _subtotal = 100;
            _tax = 10;
            _total = _subtotal + _tax;
        } 

    }
}

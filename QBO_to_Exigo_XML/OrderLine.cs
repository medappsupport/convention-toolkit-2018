﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace QBO_to_Exigo_XML
{
    class OrderLine
    {
        private string _orderid;
        private string _sku;
        private string _uom;
        private string _description;
        private decimal _listprice;
        private decimal _productprice;
        private decimal _tax;
        private int _qty;
        private decimal _total;
        private decimal _volume;
        private decimal _reward;

        public string OrderID
        {
            get { return _orderid; }
        }

        public string UOM
        {
            get { return _uom; }
        }

        public string Description
        {
            get { return _description; }
        }

        public decimal ListPrice
        {
            get { return _listprice; }
        }

        public decimal ProductPrice
        {
            get { return _productprice; }
        }

        public string SKU
        {
            get { return _sku; }
        }

        public decimal Tax
        {
            get { return (decimal)_tax; }
        }

        public int Quantity
        {
            get { return _qty; }
        }

        public decimal Total
        {
            get { return _total; }
        }

        public decimal Volume
        {
            get { return _volume; }
        }

        public decimal Reward
        {
            get { return _reward; }
        }

        public void Load(DataRow row, Product product)
        {

            string[] skucode = row["SKU"].ToString().Split('_');
            string[] nontax = { "40151NI", "JV1_EA" };

            _orderid = "C2018" + row["Num"].ToString();
            _sku = skucode[0];
            _uom = skucode[1];
            _description = product.Item;
            if (row["Qty"].ToString() == "")
            {
                _qty = 1;
            }
            else
            {
                _qty = int.Parse(row["Qty"].ToString());
            }
            //_listprice = Decimal.Parse(row["Amount"].ToString());
            if(row["Sales Price"].ToString() == "")
            {
                _listprice = 0.00M;
            }
            else
            {
                _listprice = Decimal.Parse(row["Sales Price"].ToString());
            }

            _productprice = product.ListPrice;
            //_tax = product.Tax * _qty;
            //if (product.Type == "Food") { _tax = 0.05725; }
            //else { _tax = 0.09013; }
            //decimal subtotal = _listprice * _qty;
            //_tax = Math.Round((double)subtotal * product.Tax,2,MidpointRounding.AwayFromZero);
            decimal calctax = _listprice - _listprice / (1 + (decimal)product.Tax);
            
            //if (row["SKU"].ToString() == "40151NI")
            if (nontax.Contains<string>(row["SKU"].ToString()))
            {
                _tax = 0.00M;
            }
            else
            {
                _tax = Math.Round(calctax, 2, MidpointRounding.AwayFromZero) * _qty;
            }
            _total = (_listprice * _qty);
            _volume = (_total - _tax) * product.CVIndex;
            
        }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;


namespace QBO_to_Exigo_XML
{
    class Product
    {
        private string _item;
        private string _sku;
        private decimal _listprice;
        private double _tax;
        private decimal _cvindex;
        private string _type;

        public string Item
        {
            get { return _item; }
        }

        public string SKU
        {
            get { return _sku; }
        }

        public decimal ListPrice
        {
            get { return _listprice; }
        }

        public double Tax
        {
            get { return _tax; }
        }

        public string Type
        {
            get { return _type; }
        }

        public decimal CVIndex
        {
            get { return _cvindex; }
        }

        public void Load(DataRow row)
        {
            _item = row["Item"].ToString();
            _sku = row["SKU"].ToString();
            _listprice = Decimal.Parse(row["List Price"].ToString());
            _type = row["Type"].ToString();
            //_tax = Decimal.Parse(row["Tax"].ToString());
            if (_type == "Food") { _tax = 0.05725; }
            else { _tax = 0.09013; }

            _cvindex = Decimal.Parse(row["cvIndex"].ToString());
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//using System.Data.SqlClient;
using Microsoft.Office.Interop.Excel;
using System.IO;
using System.Xml;

namespace QBO_to_Exigo_XML
{
    public partial class Form1 : Form
    {
        System.Data.DataTable salestable;
        System.Data.DataTable customertable;
        System.Data.DataTable producttable;
        System.Data.DataTable errortable;
        System.Data.DataTable xlsxtable;
        System.Data.DataView orderview;
        System.Data.DataTable workshoptable;
        DataColumn column;
        DataRow newrow;
        string[] orderview_col = { "Num" };
        System.Data.DataTable ordertable;        
        DataColumn[] key = new DataColumn[1];
        DataRow[] no_id;
        DataRow[] clients;

        CreateXML createxml = new CreateXML();
        string query;

        XLSReader reader = new XLSReader();

        List<Customer> customers = new List<Customer>();
        Customer customer;
        List<Product> products = new List<Product>();
        Product product;        
        List<Order> orders = new List<Order>();
        Order order;
        List<OrderLine> orderlines = new List<OrderLine>();
        OrderLine orderline;

        int pbarmax;
        int end;
        public Form1()
        {
            InitializeComponent();
            
        }
        

        private void button1_Click(object sender, EventArgs e)
        {


            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                query = "SELECT * FROM [Sales by Product Service Detail$]";
                reader.Read(openFileDialog1.FileName, query);
                textBox1.Text = openFileDialog1.FileName;
                
                salestable = reader.Table;
                orderview = new DataView(salestable);
                ordertable = orderview.ToTable(distinct: true, columnNames: orderview_col);

                key[0] = salestable.Columns["Line"];
                
                salestable.PrimaryKey = key;

                dataGridView1.DataSource = salestable;
                dataGridView1.Update();
            }

            foreach (DataRow row in salestable.Rows)
            {
                orderline = new OrderLine();
                Product item = new Product();
                item = products.Find(x => x.SKU == row["SKU"].ToString());

                errortable = new System.Data.DataTable();
           
                if (row["Customer"].ToString() == "")
                {
                    row["Customer"] = "Company Account";
                    //dataGridView1.Rows[int.Parse(row["Line"].ToString()) - 1].DefaultCellStyle.ForeColor = Color.ForestGreen;
                    dataGridView1.Update();
                }

                if (item == null)
                {                    
                    //errortable.Rows.Add(row);                    
                    continue;
                }

                orderline.Load(row, item);
                orderlines.Add(orderline);
            }

            //foreach(string orderid in orderlines.Select(x => x.OrderID).Distinct())
            foreach (DataRow row in ordertable.Rows)
            {
                string orderid = "C2018" + row[0].ToString();
                order = new Order();
                order.OrderId = orderid;
                DataRow[] found = salestable.Select("Num = " + row[0].ToString());
                order.OrderDate = DateTime.Parse(found[0]["Date"].ToString());
                string name = found[0]["Customer"].ToString();
                //order.OrderDate = DateTime.Parse(salestable.Rows.Find(row[0].ToString())["Date"].ToString());

                //DataRow[] found = salestable.Select("Num = " + row[0].ToString());
                //var qcustomer;
                //qcustomer = (salestable.Rows.Find(orderid)["Customer"].ToString()).Split(' ');
                // qcustomer = (found[0]["Customer"].ToString()).Split(' ');
                IEnumerable<string> qcustomer = 
                    from customer in customers
                    where (customer.FirstName + " " + customer.LastName) == name
                    select customer.CustomerId;


                order.CustomerId = qcustomer.ElementAtOrDefault(0);

                List<OrderLine> lines = new List<OrderLine>();
                //lines = orderlines.FindAll(x => x.OrderID == orderid);
                foreach(OrderLine o in orderlines)
                {
                    if (o.OrderID == orderid)
                    {
                        lines.Add(o);
                    }
                }

                Decimal[] lineprice = new Decimal[lines.Count];
                Decimal[] linetax = new Decimal[lines.Count];
                Decimal[] linetotal = new Decimal[lines.Count];

                //lineprice = lines.Select(x => x.ListPrice).ToArray();
                linetax = lines.Select(x => x.Tax).ToArray();
                linetotal = lines.Select(x => x.Total).ToArray();
                
                order.Tax = Math.Round(linetax.Sum(),2,MidpointRounding.AwayFromZero);
                order.Total = Math.Round(linetotal.Sum(), 2, MidpointRounding.AwayFromZero);
                //order.SubTotal = lineprice.Sum();
                order.SubTotal = order.Total - order.Tax;

                orders.Add(order);
            }

            dataGridView2.DataSource = orders;            
            dataGridView2.Update();
            //dataGridView2.Sort(dataGridView2.Columns[0], ListSortDirection.Ascending);

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            /*Check
            DirectoryInfo posdir = new DirectoryInfo("C:\\POS");
            DirectoryInfo xmldir = new DirectoryInfo("C:\\POS\\XML");
            FileInfo reg = new FileInfo("C:\\POS\\Convention 2018 Registration.xlsx");
            FileInfo prod = new FileInfo("C:\\POS\\Convention 2018 Products.xlsx");
            

            if (posdir.Exists == false)
            {
                posdir.Create();
            }

            if(xmldir.Exists == false)
            {
                xmldir.Create();
            }

            if(reg.Exists == false)
            {
                FileInfo copy = new FileInfo("\\\\MED-FS01\\IT Share \\ProductionSupport\\Convention_QuickBooks_to_XML\\Convention 2018 Registration.xlsx");
                copy.MoveTo(reg.FullName);
            }

            if(prod.Exists == false)
            {
                FileInfo copy = new FileInfo("\\\\MED-FS01\\IT Share\\ProductionSupport\\Convention_QuickBooks_to_XML\\Convention 2018 Products.xlsx");
                copy.MoveTo(prod.FullName);
            }
            */

            openFileDialog1.Filter = "Excel Files (*.xlsx)|*.xlsx";
            openFileDialog1.DefaultExt = ".xlsx";

            /* Adding population from registration list to List of Customer objects
             */
            reader = new XLSReader();
            reader.Read("c:\\pos\\Convention 2018 Registration.xlsx", "SELECT * FROM [Registrants$]");
            //reader.Read("\\\\MED-FS01\\IT Share\\ProductionSupport\\Convention_QuickBooks_to_XML\\Convention 2018 Registration.xlsx", "SELECT * FROM [Registrants$]");


            customertable = reader.Table;
            //Add generic customer to table
            customertable.Rows.Add("5","5","5","Company", "Account",null, null, null);

            foreach (DataRow row in customertable.Rows)
            {                
                customer = new Customer();
                customer.Load(row);
                customers.Add(customer);
            }

            

            /* 
             */
            reader = new XLSReader();
            reader.Read("c:\\pos\\Convention 2018 Products.xlsx", "SELECT * FROM [Sheet1$]");
            //reader.Read("\\\\MED-FS01\\IT Share\\ProductionSupport\\Convention_QuickBooks_to_XML\\Convention 2018 Products.xlsx", "SELECT * FROM [Sheet1$]");

            producttable = reader.Table;

            foreach (DataRow row in producttable.Rows)
            {
                product = new QBO_to_Exigo_XML.Product();
                product.Load(row);
                products.Add(product);
            }
            
        }

        private void button2_Click(object sender, EventArgs e)
        {

            pbarmax = orders.Count;
            progressBar1.Visible = true;
            progressBar1.Minimum = 1;
            progressBar1.Maximum = pbarmax;
            progressBar1.Value = 1;
            progressBar1.Step = 1;

            foreach (Order order in orders)
            {
                Customer order_customer = new Customer();
                order_customer = customers.Find(x => x.CustomerId == order.CustomerId);

                if (order_customer == null) { order_customer = customers[customers.Count - 1]; }


                List<OrderLine> order_lines = new List<OrderLine>();
                order_lines = orderlines.FindAll(x => x.OrderID == order.OrderId);

                
                    //createxml.ExigoXML(order, order_customer, order_lines);
                    createxml.NavXML(order, order_customer, order_lines);
                    progressBar1.PerformStep();
                    textBox2.Text = progressBar1.Value.ToString() + "/" + pbarmax;
                    textBox2.Update();
                
            
            }

            int len = 100;
            int i;
            int n = 0;
            

            decimal num = Math.Truncate((decimal)orders.Count/len);

            

            for (i = 0; i < (num + 1); i++)
            {
                
                if (i == num)
                {
                    end = orders.Count % len;
                }
                if (i != num)
                {
                    end = ((int)len - 1);
                }

                List<Order> orderlist = orders.GetRange((i * len), end);
                n++;
                string datestamp = DateTime.Today.ToString("yyMMMdd");
                string filename = "Convention_2018_Orders_NAV_" + n.ToString() + "_" + datestamp + ".xml";
                XmlWriterSettings xmlsettings = new XmlWriterSettings();
                xmlsettings.Indent = true;

                using (XmlWriter xmlwrite = XmlWriter.Create("\\\\MED-FS01\\IT Share\\ProductionSupport\\Convention_QuickBooks_to_XML\\XML\\Nav\\" + filename, xmlsettings))
                {
                    xmlwrite.WriteStartDocument();
                    xmlwrite.WriteStartElement("Envelope", "");
                    xmlwrite.WriteStartElement("Body");
                    xmlwrite.WriteStartElement("ordersResponse");
                    xmlwrite.WriteStartElement("navisionOrders");

                    foreach(Order order in orderlist)
                    {
                        Customer order_customer = new Customer();
                        order_customer = customers.Find(x => x.CustomerId == order.CustomerId);
                        //Default to Company Account if no customer found
                        if (order_customer == null) { order_customer = customers[customers.Count - 1]; }

                        List<OrderLine> order_lines = new List<OrderLine>();
                        order_lines = orderlines.FindAll(x => x.OrderID == order.OrderId);

                        xmlwrite.WriteStartElement("navisionOrder");

                        xmlwrite.WriteElementString("orderNo", order.OrderId);
                        xmlwrite.WriteElementString("documentType", "Order");
                        xmlwrite.WriteElementString("ORHRefID", "");

                        xmlwrite.WriteStartElement("autoShip");
                        xmlwrite.WriteElementString("shippingCode", "DNS");
                        xmlwrite.WriteElementString("shippingCodeFrequencyDays", "");
                        xmlwrite.WriteElementString("initialShipDate", "");
                        xmlwrite.WriteElementString("lastShipDate", "");
                        xmlwrite.WriteElementString("nextShipDate", "");
                        xmlwrite.WriteElementString("autoShipEnabled", "false");
                        xmlwrite.WriteElementString("autoShipType", "");
                        xmlwrite.WriteEndElement(); //End autoShip

                        xmlwrite.WriteStartElement("clientInfo");
                        xmlwrite.WriteElementString("clientNo", order_customer.CustomerId);
                        xmlwrite.WriteElementString("email", order_customer.CustomerEmail);
                        xmlwrite.WriteElementString("firstName", order_customer.FirstName);
                        xmlwrite.WriteElementString("lastName", order_customer.LastName);
                        xmlwrite.WriteElementString("phone", "");
                        xmlwrite.WriteElementString("eveningPhone", "");
                        xmlwrite.WriteElementString("weightToLose", "");
                        xmlwrite.WriteElementString("howDidYouHear", "");
                        xmlwrite.WriteElementString("shippingFirstName", order_customer.FirstName);
                        xmlwrite.WriteElementString("shippingLastName", order_customer.LastName);
                        xmlwrite.WriteElementString("shippingAddr", "701 Convention Plaza");
                        xmlwrite.WriteElementString("shippingAddr2", "");
                        xmlwrite.WriteElementString("shippingCity", "St. Louis");
                        xmlwrite.WriteElementString("shippingState", "MO");
                        xmlwrite.WriteElementString("shippingZip", "63101");
                        xmlwrite.WriteElementString("shippingCountry", "US");
                        xmlwrite.WriteElementString("billingFirstName", order_customer.FirstName);
                        xmlwrite.WriteElementString("billingLastName", order_customer.LastName);
                        xmlwrite.WriteElementString("billingAddr", "");
                        xmlwrite.WriteElementString("billingAddr2", "");
                        xmlwrite.WriteElementString("billingCity", "");
                        xmlwrite.WriteElementString("billingState", "");
                        xmlwrite.WriteElementString("billingZip", "");
                        xmlwrite.WriteElementString("billingCountry", "");
                        xmlwrite.WriteEndElement(); //End clientInfo

                        xmlwrite.WriteElementString("shipmentDate", "");
                        xmlwrite.WriteElementString("orderSource", "POS");
                        xmlwrite.WriteElementString("leadType", "POS");
                        xmlwrite.WriteElementString("custPostGrp", "TSFL");
                        xmlwrite.WriteElementString("leadsOrder", "0");
                        xmlwrite.WriteElementString("adjustments", "");
                        xmlwrite.WriteElementString("orderDateTime", order.OrderDate.ToString());

                        xmlwrite.WriteStartElement("shipping");
                        xmlwrite.WriteElementString("shipAmount", "");
                        xmlwrite.WriteElementString("shipDiscountAmount", "");
                        xmlwrite.WriteElementString("shipTax", "");
                        xmlwrite.WriteElementString("shipCode", "");
                        xmlwrite.WriteElementString("shipProcessCode", "");
                        xmlwrite.WriteEndElement(); //End shipping

                        xmlwrite.WriteElementString("taxAmount", "");
                        xmlwrite.WriteElementString("taxSource", "NO_NEXUS");
                        xmlwrite.WriteElementString("orderTotalAmount", order.Total.ToString("F2"));

                        xmlwrite.WriteStartElement("sponsorInfo");
                        xmlwrite.WriteElementString("sponsorId", "");
                        xmlwrite.WriteElementString("sponsorName", "");
                        xmlwrite.WriteElementString("sponsorEmail", "");
                        xmlwrite.WriteElementString("sponsorWebsite", "");
                        xmlwrite.WriteElementString("sponsorPhone", "");
                        xmlwrite.WriteEndElement(); //End sponsorInfo

                        xmlwrite.WriteElementString("comments", "");
                        xmlwrite.WriteElementString("packingSlipComments", "");

                        xmlwrite.WriteStartElement("paymentDetails");
                        xmlwrite.WriteStartElement("paymentDetail");
                        xmlwrite.WriteElementString("authorizationNum", "");
                        xmlwrite.WriteElementString("ccNum", "");
                        xmlwrite.WriteElementString("cvvNum", "");
                        xmlwrite.WriteElementString("paymentTerms", "CASH");
                        xmlwrite.WriteElementString("paymentMethod", "");
                        xmlwrite.WriteElementString("paymentAmount", "");
                        xmlwrite.WriteEndElement(); //End paymentDetail
                        xmlwrite.WriteEndElement(); //End paymentDetails

                        xmlwrite.WriteStartElement("orderItemLines");

                        foreach (OrderLine line in order_lines)
                        {

                            xmlwrite.WriteStartElement("orderItem");
                            xmlwrite.WriteElementString("lineItem", "");
                            xmlwrite.WriteElementString("orderItemNo", line.OrderID);
                            xmlwrite.WriteElementString("lineSku", line.SKU.Split('_')[0]);
                            xmlwrite.WriteElementString("lineQuantity", line.Quantity.ToString());
                            xmlwrite.WriteElementString("unitOfMeasure", line.UOM);
                            xmlwrite.WriteElementString("unitPrice", line.ListPrice.ToString("F2"));
                            xmlwrite.WriteElementString("cashOnAccountClientNo", "");
                            xmlwrite.WriteElementString("lineDiscount", "0");
                            xmlwrite.WriteElementString("lineTaxAmount", "0");
                            xmlwrite.WriteEndElement(); //End OrderItem
                        }

                        xmlwrite.WriteEndElement(); //End OrderItemLines
                        xmlwrite.WriteEndElement(); //End navisionOrder
                       
                    }
                    xmlwrite.WriteEndElement(); //End navisionOrders

                    xmlwrite.WriteStartElement("message");
                    xmlwrite.WriteElementString("success", "true");
                    xmlwrite.WriteElementString("code", "NV3");
                    xmlwrite.WriteElementString("description", "MaxOrders exceed orders returned - end of orders");
                    xmlwrite.WriteEndElement(); //End message

                    xmlwrite.WriteEndElement(); //End ordersResponse
                    xmlwrite.WriteEndElement(); //End Body
                    xmlwrite.WriteEndDocument();

                    //xmlwrite.Finalize;
                    xmlwrite.Flush();
                    xmlwrite.Close();
                }

                
            }
            MessageBox.Show("XML creation completed.\n" + orders.Count.ToString() + " orders processed", "XML Creator", MessageBoxButtons.OK);
        }

        private void openListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                reader = new XLSReader();
                //reader.Read(openFileDialog1.FileName, "SELECT * FROM [Registrants$]");
                reader.Open(openFileDialog1.FileName);
            }

            xlsxtable = reader.Table;
            textBox1.Text = openFileDialog1.FileName;
            
            dataGridView1.DataSource = xlsxtable;
            dataGridView1.Update();
        }

        private void validateToolStripMenuItem_Click(object sender, EventArgs e)
        {

            int index;

            if (xlsxtable.Rows.Count > 1)
            {
                no_id = xlsxtable.Select("[Health Coach ID] IS NULL");
                clients = xlsxtable.Select("[Registration Type] = 'Client'");        
            }

            foreach(DataRow row in clients)
            {
                index = xlsxtable.Rows.IndexOf(row);
                dataGridView1.Rows[index].DefaultCellStyle.ForeColor = Color.Green;
                Customer lookup = new Customer();
                lookup.LookupByEmail(row);

                if (lookup.CustomerId != row["Health Coach ID"].ToString() && lookup.CustomerId != null)
                {
                    row["Health Coach ID"] = lookup.CustomerId;
                    
                }
            }

            foreach(DataRow row in no_id)
            {

                index = xlsxtable.Rows.IndexOf(row);
                dataGridView1.Rows[index].DefaultCellStyle.ForeColor = Color.Blue;

                if (row["Registration Type"].ToString() != "Guest")
                {
                    Customer lookup = new QBO_to_Exigo_XML.Customer();
                    lookup.LookupByEmail(row);

                    if(lookup.CustomerId != null)
                    {
                        row["Health Coach ID"] = lookup.CustomerId;
                    }

                    else
                    {
                        lookup = new QBO_to_Exigo_XML.Customer();
                        lookup.LookupByName(row);
                        if (lookup.CustomerId != null)
                        {
                            row["Health Coach ID"] = lookup.CustomerId;
                        }
                        else
                        {
                            row["Health Coach ID"] = 99999999;
                            dataGridView1.Rows[index].DefaultCellStyle.ForeColor = Color.Red;
                        }

                    }
                }
                
                if (row["Registration Type"].ToString() == "Guest")
                {
                    row["Health Coach ID"] = 5;
                }
                               
                
            }

            if(xlsxtable.Columns.Contains("Source ID"))
            {
                foreach (DataRow row in xlsxtable.Rows)
                {
                    if(row["Source ID"].ToString() == "")
                    {
                        row["Source ID"] = row["Health Coach ID"].ToString();
                    }                    
                }
            }
            


        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog savefile = new SaveFileDialog();
            savefile.Filter = "Excel Files (*.xlsx)|*.xlsx";
            savefile.DefaultExt = ".xlsx";
            XLSWriter xlsout = new XLSWriter();

            string sheetname = "Sheet1";

            if (dataGridView1.DataSource == xlsxtable)
            {
                sheetname = "Registrants";
            }

            if (savefile.ShowDialog() == DialogResult.OK)
            {
                xlsout.WriteXlsx(xlsxtable, savefile.FileName, sheetname);
            }
        }

        private void viewListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = customertable;
            dataGridView1.Update();
        }

        private void getWorkshopToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //string track;
            workshoptable = new System.Data.DataTable();

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "First Name";
            workshoptable.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Last Name";
            workshoptable.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Email Address";
            workshoptable.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Registration";
            workshoptable.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "Rank";
            workshoptable.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Workshop Track";
            workshoptable.Columns.Add(column);

            /*foreach (Customer customer in customers)
            {
                int rank = customer.GetWorkshop(customer.CustomerId);

                
                else
                {
                    newrow["Workshop Track"] = "Executive Director & Above";
                }
                    
                workshoptable.Rows.Add(newrow);
            }*/
            pbarmax = xlsxtable.Rows.Count;
            progressBar1.Visible = true;
            progressBar1.Minimum = 1;
            progressBar1.Maximum = pbarmax;
            progressBar1.Value = 1;
            progressBar1.Step = 1;


            foreach (DataRow row in xlsxtable.Rows)
            {
                try {
                    if (row["Registration Type"].ToString() == "Guest")
                    {
                        row["Source ID"] = "5";
                        row["Client ID"] = "5";
                        row["Health Coach ID"] = DBNull.Value;

                        newrow = workshoptable.NewRow();
                        newrow["First Name"] = row["First Name"].ToString();
                        newrow["Last Name"] = row["Last Name"].ToString();
                        newrow["Email Address"] = row["Email Address"].ToString();
                        newrow["Registration"] = row["Confirmation Number"];
                        newrow["Rank"] = DBNull.Value;
                        newrow["Workshop Track"] = "Coach - Director";
                        workshoptable.Rows.Add(newrow);

                    }

                    if (row["Registration Type"].ToString() == "Client")
                    {
                        Customer client = new Customer();
                        client.LookupByName(row);
                        string clientid;

                        if (client.CustomerId == "")
                        {
                            clientid = row["Client ID"].ToString();
                            if (row["Client ID"].ToString() == "")
                            {
                                clientid = DBNull.Value.ToString();
                            }
                        }
                        else
                        {
                            clientid = client.CustomerId;
                        }
                        /*if (clientid == null)
                        {
                            clientid = DBNull.Value.ToString();
                        }*/
                        row["Client ID"] = clientid;

                        if ((row["Source ID"].ToString() != "") && (row["Client ID"].ToString() == ""))
                        {
                            row["Source ID"] = row["Source ID"].ToString();
                        }
                        else
                        {
                            row["Source ID"] = Double.Parse(clientid);
                        }

                        row["Health Coach ID"] = DBNull.Value;

                        newrow = workshoptable.NewRow();
                        newrow["First Name"] = row["First Name"].ToString();
                        newrow["Last Name"] = row["Last Name"].ToString();
                        newrow["Email Address"] = row["Email Address"].ToString();
                        newrow["Registration"] = row["Confirmation Number"];
                        newrow["Rank"] = DBNull.Value;
                        newrow["Workshop Track"] = "Coach - Director";
                        workshoptable.Rows.Add(newrow);
                    }

                    if (row["Registration Type"].ToString().Contains("Coach"))
                    {
                        Customer coach = new Customer();
                        /*coach.LookupByEmail(row);                 
                        if (coach.CustomerId == "")
                        {
                            coach.LookupByName(row);
                        }*/
                        coach.Load(row);
                        if (coach.CustomerId == "")
                        {
                            coach.LookupByEmail(row);

                            if (coach.CustomerId == "")
                            {
                                coach.LookupByName(row);
                            }
                        }
                        int rank = coach.GetWorkshop(coach.CustomerId);
                        string workshop;
                        if (rank < 50)
                        {
                            workshop = "Coach - Director";
                        }
                        else
                        {
                            workshop = "Executive Director & Above";
                        }

                        row["Client ID"] = DBNull.Value;
                        row["Health Coach ID"] = coach.CustomerId;
                        row["Source ID"] = Double.Parse(coach.CustomerId);


                        newrow = workshoptable.NewRow();
                        newrow["First Name"] = row["First Name"].ToString();
                        newrow["Last Name"] = row["Last Name"].ToString();
                        newrow["Email Address"] = row["Email Address"].ToString();
                        newrow["Registration"] = row["Confirmation Number"];
                        newrow["Rank"] = rank.ToString();
                        newrow["Workshop Track"] = workshop;
                        workshoptable.Rows.Add(newrow);
                    }
                    progressBar1.PerformStep();
                    textBox2.Text = progressBar1.Value.ToString() + "/" + pbarmax.ToString();
                }
                catch
                {
                    continue;
                }
            }

            dataGridView1.DataSource = workshoptable;
            dataGridView1.Update();

            SaveFileDialog savefile = new SaveFileDialog();
            savefile.Filter = "Excel Files (*.xlsx)|*.xlsx";
            savefile.DefaultExt = ".xlsx";
            XLSWriter xlsout = new XLSWriter();

            /*if (savefile.ShowDialog() == DialogResult.OK)
            {
                xlsout.WriteXlsx(workshoptable, savefile.FileName, "Registrants");
            }*/

            string datestamp = DateTime.Now.ToString("yyMMMdd_hhmm");
            xlsout.WriteXlsx(workshoptable, "C:\\Convention_POS\\Convention_2018_Workshops" + datestamp + ".xlsx", "Workshops");
            xlsout.WriteXlsx(xlsxtable, "C:\\Convention_POS\\Convention_2018_Updated_SourceID" + datestamp + ".xlsx", "Registrants");
        }

        private void qBOCheckToolStripMenuItem_Click(object sender, EventArgs e)
        {
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Quickbooks";
            customertable.Columns.Add(column);

            key = new DataColumn[1];
            key[0] = xlsxtable.Columns["Customer"];

            xlsxtable.PrimaryKey = key;

            pbarmax = customertable.Rows.Count;
            progressBar1.Visible = true;
            progressBar1.Minimum = 1;
            progressBar1.Maximum = pbarmax;
            progressBar1.Value = 1;
            progressBar1.Step = 1;

            foreach (DataRow row in customertable.Rows)
            {
                string name = row["First Name"].ToString() + " " + row["Last Name"].ToString();
                string expression = "Customer = \'" + name.Replace('\'',' ') + "\'" ;

                DataRow[] qbrecord = xlsxtable.Select(expression);

                if (qbrecord.Length == 0)
                {
                    row["Quickbooks"] = "ADD";
                    progressBar1.PerformStep();
                    textBox2.Text = progressBar1.Value.ToString() + "/" + pbarmax.ToString();
                    continue;
                }

                if (row["Source ID"].ToString() == qbrecord[0]["Company"].ToString())
                {
                    row["Quickbooks"] = "MATCH";
                }

                if (row["Source ID"].ToString() != qbrecord[0]["Company"].ToString())
                {
                    row["Quickbooks"] = "UPDATE";
                }

                

                progressBar1.PerformStep();
                textBox2.Text = progressBar1.Value.ToString() + "/" + pbarmax.ToString();

            }

            dataGridView1.DataSource = customertable;
            dataGridView1.Update();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.OleDb;

namespace QBO_to_Exigo_XML
{
    public class XLSReader
    {
        /* This is a reuseable class with the purpose reading contents of an
         * Excel file and returning a DataTable
         */
        private string _excelconn;
        private System.Data.DataTable _table;
        private DataSet _dataset;
        private OleDbConnection _oleconn;
        private OleDbCommand _olecomm;
        private OleDbDataAdapter _oleadapter;        

        public System.Data.DataTable Table
        {
            get { return _table; }
        }

        public void Open(string file)
        {
            Application excel = new Application();
            
            Workbook workbook = excel.Workbooks.Open(file);
            
            //Worksheet worksheet = workbook.Worksheets[0];
            string sheetname = workbook.Worksheets.Item[1].Name;
            string command = "SELECT * FROM [" + sheetname + "$]";



            _excelconn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + file + ";Extended Properties=\"Excel 12.0 Xml; HDR=Yes; IMEX=1\"";
            _oleconn = new OleDbConnection(_excelconn);
            _olecomm = new OleDbCommand(command, _oleconn);
            _oleadapter = new OleDbDataAdapter(_olecomm);
            _table = new System.Data.DataTable();
            _dataset = new DataSet();

            _oleconn.Open();
            _oleadapter.Fill(_dataset);
            _oleconn.Close();
            _table = _dataset.Tables[0];

            
        }
        public void Read(string file,string command)
        {


            _excelconn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + file + ";Extended Properties=\"Excel 12.0 Xml; HDR=Yes; IMEX=1\"";
            _oleconn = new OleDbConnection(_excelconn);
            _olecomm = new OleDbCommand(command,_oleconn);
            _oleadapter = new OleDbDataAdapter(_olecomm);
            _table = new System.Data.DataTable();
            _dataset = new DataSet();

            _oleconn.Open();            
            _oleadapter.Fill(_dataset);
            _oleconn.Close();
            _table = _dataset.Tables[0];
        }
    }

}
